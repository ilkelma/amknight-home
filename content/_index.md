---
title: "A.M. Knight"
featured_image: '/images/hops.jpg'
description: "Infrastructure & Software Engineer"
---
<div class="dt mw6 center pt0 pb5 pv5-m pv6-ns">
  <div class="db dtc-ns v-mid-ns">
    <img src="/images/red_rock.jpg" alt="Photo of A.M. Knight smiling at the camera in Red Rock coffee shop in Mountain View, California in 2018" class="w-100 mw7 w5-ns" />
  </div>
  <div class="db dtc-ns v-mid ph2 pr0-ns pl3-ns">
    <div class="lh-copy">
      <p>
        Welcome to my personal landing page on the web.
      </p>
      <p>
        At present this page contains my extremely rarely updated blog <a href="/posts">2b || !2b</a> and links to my <a href="https://cv.amknight.com"> CV</a>. 
      </p>
      <p>
        The code for the site can be found in <a href="https://www.gitlab.com/ilkelma/amknight-home">this repo</a> alongside the gitlab pipelines that publish it to gitlab pages.
      </p>
    </div>
  </div>
</div>